return {
  cmd = { 'perlnavigator' },
  settings = {
    perlnavigator = {
      perlPath = 'perl',
      enableWarnings = true,
      perltidyProfile = '',
      perlcriticProfile = '$workspaceFolder/.perlcriticrc',
      perlcriticEnabled = true,
    },
  },
}
