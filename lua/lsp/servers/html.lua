return {
  filetypes = { 'html', 'javascript', 'typescript', 'markdown' },
  init_options = { provideFormatter = false },
}
