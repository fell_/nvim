-- used solely for keyword completions
return {
  settings = {
    pls = {
      perlcritic = { enabled = false },
      syntax = { enabled = false },
    },
  },
}
