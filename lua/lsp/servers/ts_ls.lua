return {
  init_options = {
    hostInfo = 'neovim',
    preferences = {
      quotePreference = 'single',
    },
  },
}
