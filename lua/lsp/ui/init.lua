-- inspired by glepnir's lspsaga: https://github.com/nvimdev/lspsaga.nvim

return {
  cda = require('lsp.ui.code_action'),
  def = require('lsp.ui.definition'),
  dgn = require('lsp.ui.diagnostic'),
  ren = require('lsp.ui.rename'),
  sig = require('lsp.ui.signature_help'),
}
