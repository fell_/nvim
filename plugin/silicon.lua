if vim.fn.executable('silicon') ~= 1 then
  return
end

local dir = os.getenv('HOME') .. '/Media/Pictures/Screenshots/Code/'
local colours = { 'e67e80', 'a7c080', 'dbbc7f', '7fbbb3', 'd699b6', '83c092' }

L.key.vnmap('<leader>*', function()
  local ft = vim.o.filetype
  local file = ('silicon_%s.png'):format(os.date('%Y%m%d-%H%M%S'))
  local args = {
    '-l ' .. ft,
    '-o ' .. dir .. file,
    "-f 'Ellograph CF'",
    ("-b '#%s'"):format(colours[math.random(1, #colours)]),
    '--shadow-offset-x 4',
    '--shadow-offset-y 4',
    '--shadow-blur-radius 6',
    "--shadow-color '#374247'",
    '--no-window-controls',
    '--theme everforest_dark',
  }

  -- get selection contents
  local start_ln, end_ln = vim.fn.getpos('v')[2], vim.fn.getpos('.')[2]
  local sel = start_ln < end_ln and vim.fn.getline(start_ln, end_ln)
    or vim.fn.getline(end_ln, start_ln)
  ---@cast sel -string

  -- escape substrings
  for i, s in pairs(sel) do
    local mut = s:gsub('\\', '\\\\')
    mut = mut:gsub('"', '\\"')
    mut = mut:gsub('`', '\\`')
    mut = mut:gsub('!', '\\!')
    mut = mut:gsub('%$', '\\$')
    sel[i] = mut
  end

  -- run silicon
  local ret = os.execute(
    ('printf \'%%s\' "%s" | silicon %s'):format(
      table.concat(sel, '\n'),
      table.concat(args, ' ')
    )
  )

  assert(ret == 0, "silicon: couldn't create file")
  vim.notify('silicon: saved as ' .. file, vim.log.levels.INFO)
end)
